package me.samirmsallem.objects;

import processing.core.PGraphics;

public interface ObjectProperties {

	public void draw(PGraphics g);
	public int getX();
	public void setX(int x);
	public int getY();
	public void setY(int y);
	
}
