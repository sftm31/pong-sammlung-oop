package me.samirmsallem.objects;
import java.util.Random;
import processing.core.PGraphics;

public class Ball implements ObjectProperties{
    public int x;
	public int y;
    public int velocity_x, velocity_y;
    public int color;
    static Random r = new Random();
    int widthBound, heightBound;
    int ball_width = 40; 
    int ball_height = 40;
    public boolean hitWall = false;
    int dX;
	int dY;
 
    public Ball(int color, int width, int height, int widthBound, int heightBound) { //pong
    	this.color = color;
    	this.ball_width = width;
    	this.ball_height = height;
    	this.widthBound = widthBound;
    	this.heightBound = heightBound;
    	this.setCentered();
    }
    
    public int getX() {
		return this.x;
	}
	public void setX(int x) {
		this.x = x;
		
	}
	public int getY() {
		return this.y;
	}
	public void setY(int y) {
		this.y = y;
		
	}
	
	
    public void startPos() {
        this.x = (int)(Math.random() * this.widthBound);
        this.y = (int)(Math.random() * this.heightBound);
        if(this.x > 580 || this.x < 20) {
        	startPos();
        }
        if(this.y > 580 || this.y < 20) {
        	startPos();
        }
    }
    public void Hit(Ball[] array) {
    	for(int i = 0; i<array.length; i++) {
    		dX = this.x-array[i].x;
    		dY = this.y-array[i].y;
    		
    		double distance = Math.sqrt(dX*dX + dY*dY);
    		if(distance <= getSize()) {
    			this.velocity_x =-this.velocity_x;
    			this.velocity_y =-this.velocity_y;
    			array[i].velocity_x = -array[i].velocity_x;
    			array[i].velocity_y = -array[i].velocity_y;
    			
    		}
    	}
    }
    public void setCentered() { 
    	this.x = widthBound/2;
    	this.y = heightBound/2;
    }
    public void movement() {
        this.x += this.velocity_x;
        this.y += this.velocity_y;
    }
    public void onAction() {
    	 if(this.x > 680  || this.x < 20) {
         	this.velocity_x = -this.velocity_x;
         	this.hitWall = true;
         }
         if(this.y > 580  || this.y < 20) {
         	this.velocity_y = -this.velocity_y;
         }
    }
    
    public void move() {
    	movement();
    	onAction();
    }
    public void draw(PGraphics g) {
    	g.fill(this.color);
        g.ellipse(this.x, this.y, this.ball_width, this.ball_height);
    }
	public void setSize(int r) {
		this.ball_width = r;
		this.ball_height = r;
	}
	
	public double getSize() {
		return this.ball_height/2 + this.ball_width/2;
	}

	public void pongDir() { 
		if(r.nextInt(2) == 1) {
    		this.velocity_x = 4;
    	} else {
    		this.velocity_x = -4;
    	}
	}
	
	
}