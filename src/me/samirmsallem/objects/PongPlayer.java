package me.samirmsallem.objects;

import processing.core.PApplet;
import processing.core.PGraphics;

public class PongPlayer extends PApplet implements ObjectProperties{

	public int x;
	public int y;
	public int shift_speed = 20;
	public int width;
	public int height;
	public int score;
	
	public PongPlayer(int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void draw(PGraphics g) {

		g.rect(this.x, this.y, width, height);
    }
	
	public void hitBall(Ball b) {
		b.velocity_x = -b.velocity_x;
     	b.velocity_y = -b.velocity_y;
     	if(b.velocity_x < 6 && b.velocity_x > 0) {
			 b.velocity_x++;
		 } if(b.velocity_x > -6 && b.velocity_x < 0) {
			 b.velocity_x--;
		 }
     	if(b.y > this.y+50 || b.y < this.y+50) {
    		 if(b.velocity_y == 0) {
    			 b.velocity_y = 4;
    		 }
    		 
    		 if(b.y < this.y+50) {
         		 b.velocity_y = -b.velocity_y;
         	 }
    		 
    	 }
     	 
     	 
	}

	public void moveUp() {
		this.y -= shift_speed;
	}
	public void moveDown() {
		this.y += shift_speed;
	}

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
}
