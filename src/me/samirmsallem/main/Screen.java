package me.samirmsallem.main;

import me.samirmsallem.gamemodes.ComGame;
import me.samirmsallem.gamemodes.Pong;
import me.samirmsallem.gamemodes.SoloGame;
import processing.core.PApplet;
import processing.core.PImage;

public class Screen extends PApplet{

	public static void main(String[] args) {
        PApplet.main(Screen.class);
    }
	public int width = 700;
	public int height = 600;
	boolean menu = true;
	int gamemode;
	public PImage img;
	
    public void settings() {
        super.size(width, height);
        img = loadImage("bg.jpg");
        
    }
    public void setup() {
		noStroke();
		smooth();
		super.background(img);
		
    }
    
    public void draw() {
    	super.background(img);
    	if(menu) {
    		textSize(32);
    		text("Bitte w�hle einen Spielmodus", 130, 50);
    		text("1: Einzelspieler ohne Com", 100, 200);
    		text("2: Multiplayer (W/S, UP/DOWN)", 100, 300);
    		text("3: Einzelspieler gegen Com (W/S)", 100, 400);
    	}

    }
	
	public void keyPressed() {
    		if(menu) {
    			if(keyCode == '1') {
    				gamemode = 1;
    				menu = false;
    			}
    			if(keyCode == '2') {
    				gamemode = 2;
    				menu = false;
    			}
    			if(keyCode == '3') {
    				gamemode = 3;
    				menu = false;
    			}
    			System.out.println("Selected Gamemode: "+gamemode);
    		}
        	if(!menu) {
        		background(0);
        		switch(gamemode) {
        		case 1: 
        			PApplet.main(SoloGame.class);
        			break;
        		case 2:
        			PApplet.main(Pong.class);
        			break;
        		case 3:
        			PApplet.main(ComGame.class);
        			break;
        		}
        	}
		}	
	}

