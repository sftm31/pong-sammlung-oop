package me.samirmsallem.gamemodes;

public interface GamemodeOptions {

	public void resetScore();
	public void reset();
	public void draw();
	public int randomizeColor();
	public void drawText();
	
}
