package me.samirmsallem.gamemodes;

import java.util.Random;

import me.samirmsallem.main.Screen;
import me.samirmsallem.objects.Ball;
import me.samirmsallem.objects.PongPlayer;

public class Pong extends Screen implements GamemodeOptions{

	Screen s;
	public boolean hit = false;
	Random r = new Random();
	PongPlayer p1;
	PongPlayer p2;
	public Ball b;
	public int round = 1;
	
	public void settings() {
		size(width, height);
	}
	
    public void setup() {
    	s = new Screen();
    	p1 = new PongPlayer(20, (super.height/2)-50, 20, 120);
    	p2 = new PongPlayer(super.width-40, (super.height/2)-50, 20, 120);
    	b = new Ball(randomizeColor(),20, 20, super.width, super.height);
    	b.pongDir();
    }
    public void draw() {
    	background(0);
    	p1.draw(g);
    	p2.draw(g);
    	b.movement();
    	b.draw(g);
    	drawText();
    	onAction(b, p1, p2); //
    	if(hit) {
    		b.color = randomizeColor();
    		hit = false;
    	}
    	
    }
    public void drawText() {
    	textSize(32);
    	text(p1.score, 40, 60);
    	text(p2.score, width-60, 60);
    }
    public int randomizeColor() {
		return color(r.nextInt(256), r.nextInt(256), r.nextInt(256));
	}
    public void keyPressed() {
			if(keyCode == 'W' && p1.y > 5) {
			p1.moveUp();
			}
			if(keyCode == 'S' && p1.y < super.height-p1.height) {
				p1.moveDown();
			}
			if(keyCode == UP && p2.y > 10) {
				p2.moveUp();
			}
			if(keyCode == DOWN && p2.y < super.height-p2.height) {
				p2.moveDown();
			}
			
    }
	 
    public void reset() {
     	b.setCentered();
     	b.pongDir();
     	b.velocity_y = 0;
     	b.velocity_x = -b.velocity_x;
     	p1.y = (this.height/2)-50;
     	p2.y = (this.height/2)-50;
	}
	
	public void onAction(Ball b, PongPlayer p1, PongPlayer p2) {
		if(b.x <= 50 && b.y >= p1.y && b.y <= p1.y+100) {
         	 p1.hitBall(b);
         	 b.velocity_y = -b.velocity_y;
         	 this.hit = true;
        }
		if(b.x >= width-50 && b.y >= p2.y && b.y <= p2.y+100) {
        	p2.hitBall(b);
        	b.velocity_y = -b.velocity_y;
        	this.hit = true;
         }
   	
		if(b.x > width-30) {
   		this.round++;
   		p1.score++;
   		reset();
        }
		if(b.x < 0) {
   		this.round++;
   		p2.score++;
   		reset();
        }
   	
        if(b.y > height-20  || b.y < 20) {
        	b.velocity_y = -b.velocity_y;
        	this.hit = true;
        }
		
	}

	public void resetScore() {
		p1.score = 0;
		p2.score = 0;
	}

}