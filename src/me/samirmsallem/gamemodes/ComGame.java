package me.samirmsallem.gamemodes;

import java.util.Random;

import me.samirmsallem.main.Screen;
import me.samirmsallem.objects.Ball;
import me.samirmsallem.objects.Com;
import me.samirmsallem.objects.PongPlayer;

public class ComGame extends Screen implements GamemodeOptions{

	Screen s;
	PongPlayer p;
	Com c;
	Ball b;
	Random r = new Random();
	public int round = 1;
	public boolean hit = false;
	

	public void reset() {
		b.setCentered();
     	b.pongDir();
     	b.velocity_y = 0;
     	b.velocity_x = -b.velocity_x;
     	p.y = (this.height/2)-50;
     	c.y = (this.height/2)-50;
	}
	
	public void settings() {
		size(width, height);
	}

	public void setup() {
    	s = new Screen();
    	p = new PongPlayer(20, (s.height/2)-50, 20, 120);
    	c = new Com(super.width-40, (s.height/2)-50, 20, 120); //
    	b = new Ball(randomizeColor(),20, 20, s.width, super.height);
    	b.pongDir();
    }
	public int randomizeColor() {
		return color(r.nextInt(256), r.nextInt(256), r.nextInt(256));
	}
	 public void draw() {
	    	background(0);
	    	p.draw(g);
	    	c.draw(g);
	    	c.movement(b);
	    	b.movement();
	    	b.draw(g);
	    	drawText();
	    	onAction(b, p, c); //
	    	if(hit) {
	    		b.color = randomizeColor();
	    		hit = false;
	    	}
	    	
	    }
	    public void drawText() {
	    	textSize(32);
	    	text(p.score, 40, 60);
	    	text(c.score, width-60, 60);
	    }
	    
		public void onAction(Ball b, PongPlayer p, Com c) {
			if(b.x <= 50 && b.y >= p.y && b.y <= p.y+100) {
	         	 p.hitBall(b);
	         	 b.velocity_y = -b.velocity_y;
	         	 this.hit = true;
	        }
			if(b.x >= width-50 && b.y >= c.y && b.y <= c.y+100) {
	        	c.hitBall(b);
	        	b.velocity_y = -b.velocity_y;
	        	this.hit = true;
	         }
	   	
			if(b.x > width-30) {
	   		this.round++;
	   		p.score++;
	   		reset();
	        }
			if(b.x < 0) {
	   		this.round++;
	   		c.score++;
	   		reset();
	        }
	   	
	        if(b.y > height-20  || b.y < 20) {
	        	b.velocity_y = -b.velocity_y;
	        	this.hit = true;
	        }
			
		}

		public void resetScore() {
			p.score = 0;
			c.score = 0;
		}
		
		public void keyPressed() {
			if(keyCode == 'W' && p.y > 5) {
			p.moveUp();
			}
			if(keyCode == 'S' && p.y < super.height-p.height) {
				p.moveDown();
			}
			
    }

	
	
	
}
